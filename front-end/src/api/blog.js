/**
 * Mocking client-server processing
 */
import axios from 'axios'
const api = axios.create({
    baseURL: 'http://localhost:3000/api',
})


const getAllMovies = () => api.get(`/blog`)

var _blogs = [];

getAllMovies().then(blogs => {
    _blogs = blogs.data.data
});


const TIMEOUT = 500

export default {
    getBlogs: (cb, timeout) => setTimeout(() => cb(_blogs), timeout || TIMEOUT),
}
