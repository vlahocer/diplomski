import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:3000/api',
})

export const getAllProducts = () => api.get(`/products`)
export const getProductById = id => api.get(`/product/${id}`)

const apis = {
    getAllProducts,
    getProductById,
}

export default apis