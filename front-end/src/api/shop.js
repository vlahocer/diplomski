/**
 * Cnodlient-server processing
 */

    import axios from 'axios'
    const api = axios.create({
        baseURL: 'http://localhost:3000/api',
    })


    const getAllProducts = () => api.get(`/products`)

    var _products = [];

    getAllProducts().then(products => {
        _products = products.data.data
    });


    const TIMEOUT = 500

    export default {
        getProducts: (cb, timeout) => setTimeout(() => cb(_products), timeout || TIMEOUT),
        buyProducts: (payload, cb, timeout) => setTimeout(() => cb(), timeout || TIMEOUT)
    }
