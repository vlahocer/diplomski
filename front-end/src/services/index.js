// Get Unique Brands from Json Data
export const getBrands = (products) => {
    var uniqueBrands = [];
    products.map((product, index) => {
        if (product.tags) {
            product.tags.map((tag) => {
                if (uniqueBrands.indexOf(tag) === -1) {
                    uniqueBrands.push(tag);
                }
            })
        }
    })
    return uniqueBrands;
}
// Get Unique Categories from Json Data
export const getCategories = (products) => {
    var uniqueCategories = [];
    products.map((product, index) => {
        if (product.category) {
            product.category.map((cat) => {
                if (uniqueCategories.indexOf(cat) === -1) {
                    uniqueCategories.push(cat);
                }
            })
        }
    })
    return uniqueCategories;
}
// Get Unique Colors from Json Data
export const getColors = (products) => {
    var uniqueColors = [];
    products.map((product, index) => {
        if(product.colors) {
            product.colors.map((color) => {
                if (uniqueColors.indexOf(color) === -1) {
                    uniqueColors.push(color);
                }
            })
        }
    })
    return uniqueColors;
}
// Get Minimum and Maximum Prices from Json Data
export const getMinMaxPrice = (products) => {
    let min = 100, max = 1000;

    products.map((product, index) => {
        let v = product.price;
        min = (v < min) ? v : min;
        max = (v > max) ? v : max;
    })

    return {'min':min, 'max':max};
}
export const getVisibleproducts = (data, { brand, color, value,category, sortBy }) => {
    return data.products.filter(product => {
        let brandMatch;
        if(brand && brand.length > 0 && product.tags) {
            for (const [index, value] of brand.entries()) {
                if(product.tags.includes(value)){
                    brandMatch = true;
                    break;
                }else{
                    brandMatch = false;
                }
            }
        }else {
            brandMatch = true;
        }
        let categoryMatch;
        if(category && category.length > 0 && product.category){
            for (const [index, value] of category.entries()) {
                if(product.category.includes(value)){
                    categoryMatch = true;
                    break;
                }else{
                    categoryMatch = false;
                }
            }
        }else{
            categoryMatch = true;
        }
        let colorMatch;
        if(color && color.length > 0 && product.colors) {
            for (const [index, value] of color.entries()) {
                if(product.colors.includes(value)){
                    colorMatch = true;
                    break;
                }else{
                    colorMatch = false;
                }
            }
        }else{
            colorMatch = true;
        }
        let startPriceMatch;
        let endPriceMatch;
        if(value) {
            startPriceMatch = typeof value.min !== 'number' || value.min <= product.price;
            endPriceMatch = typeof value.max !== 'number' || product.price <= value.max;
        }else{
            startPriceMatch = true;
            endPriceMatch = true;
        }
        return brandMatch && categoryMatch && colorMatch && startPriceMatch && endPriceMatch;
    }).sort((product1, product2) => {
        if (sortBy === 'HighToLow') {
            return product2.price < product1.price ? -1 : 1;
        } else if (sortBy === 'LowToHigh') {
            return product2.price > product1.price ? -1 : 1;
        } else if (sortBy === 'Newest') {
            return product2._id < product1._id ? -1 : 1;
        } else if (sortBy === 'AscOrder') {
            return product1.name.localeCompare(product2.name);
        } else if (sortBy === 'DescOrder') {
            return product2.name.localeCompare(product1.name);
        } else{
            return product2._id > product1._id ? -1 : 1;
        }
    });
}

export const getFilteredProducts = (data, brand = false, category = false, color = false) => {
    // console.log(brand,category,color)
    return data.filter(product => {
        let brandMatch;
        if(brand && brand.length > 0 && product.tags) {
            for (const [index, value] of brand.entries()) {
                if(product.tags.includes(value)){
                    brandMatch = true;
                    break;
                }else{
                    brandMatch = false;
                }
            }
        }else {
            brandMatch = true;
        }
        let categoryMatch;
        console.log(brand.length, category.length, color.length)
        if(category && category.length > 0 && product.category){
            for (const [index, value] of category.entries()) {
                if(product.category.includes(value)){
                    categoryMatch = true;
                    break;
                }else{
                    categoryMatch = false;
                }
            }
        }else{
            categoryMatch = true;
        }
        let colorMatch;
        if(color && color.length > 0 && product.colors) {
            for (const [index, value] of color.entries()) {
                if(product.colors.includes(value)){
                    colorMatch = true;
                    break;
                }else{
                    colorMatch = false;
                }
            }
        }else{
            colorMatch = true;
        }
        return brandMatch && categoryMatch && colorMatch;
    })
}

export const getCartTotal = cartItems => {
    var total = 0;
    for(var i=0; i<cartItems.length; i++){
        total += parseInt(cartItems[i].qty, 10)*parseInt((cartItems[i].price - cartItems[i].price*cartItems[i].discount/100), 10);
    }

    return total;
}

// Get Trending Tag wise Collection
export const getTrendingTagCollection = (products, type, tag) => {
    const items = products.filter(product => {
        return product.category === type && product.tags.includes(tag);
    })
    return items.slice(0,8)
}

// Get Trending Collection
export const getTrendingCollection = (products, type) => {
    const items = products.filter(product => {
        return product.category === type;
    })
    return items.slice(0,8)
}

// Get New Products
export const getNewProducts = (products, type) => {
    const items = products.filter(product => {
        return product.new === true;
    })

    return items.slice(0,8)
}

// Get Related Items
export const getRelatedItems = (products, type) => {
    const items = products.filter(product => {
        return product.category === type;
    })

    return items.slice(0,4)

}

// Get Best Seller
export const getBestSellerProducts = (products, type) => {
    const items = products.filter(product => {
        return product.sale === true;
    })

    return items.slice(0,8)
}

// Get Featured Broducts
export const getFeaturedProducts = (products, type) => {
    const items = products.filter(product => {
        return product.featured === true;
    })

    return items.slice(0,8)
}

// Get Best Seller
export const getBestSeller = products => {
    const items = products.filter(product => {
        return product.sale === true;
    })

    return items.slice(0,8)
}


// Get Single Product
export const getSingleItem = (products, id) => {
    const items = products.find((element) => {
        return element._id === id;
    })
    return items;
}

// Get Highest Discount
export const getHighestDiscount = (products, category) => {
    var discount = 0;
    const items = products.filter((pd) => {
        return pd.category == category;
    })
    if(items.length > 1) {
        for (var i = 0; i < items.length - 1; i++) {
            if (items[i].discount < items[i + 1].discount && discount < items[i + 1].discount) {
                discount = items[i + 1].discount;
            }
        }
    }else if(items.length > 0){
        discount = items[0].discount;
    }else{
        discount = 0;
    }
    return discount
}


