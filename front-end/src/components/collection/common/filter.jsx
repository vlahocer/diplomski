import React, {Component} from 'react';
import { connect } from 'react-redux'
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import {SlideToggle}  from 'react-slide-toggle';


import {
    getBrands,
    getColors,
    getMinMaxPrice,
    getCategories,
    getFilteredProducts
} from '../../../services';
import {filterBrand, filterColor, filterPrice, filterCategory} from '../../../actions'
import withTranslate from "react-redux-multilingual/lib/withTranslate";

class Filter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openFilter: false,
            brands: this.props.brands,
            categories: this.props.categories,
            colors: this.props.colors,
            prices: this.props.prices,
            products: this.props.products
        }
    }
    closeFilter = () => {
        document.querySelector(".collection-filter").style = "left: -365px";
    }
    clickBrandHandle(event, brands, categories, colors) {
        var index = brands.indexOf(event.target.value);
        if (event.target.checked)
            brands.push(event.target.value); // push in array checked value
        else
            brands.splice(index, 1); // removed in array unchecked value
        var products = [];
        products = getFilteredProducts(this.props.products, brands, categories, colors)
        console.log(products)
        this.props.filterBrand(brands);
    }
    clickCategoryHandle(event, categories, brands, colors) {
        var index = categories.indexOf(event.target.value);
        if (event.target.checked)
            categories.push(event.target.value); // push in array checked value
        else
            categories.splice(index, 1); // removed in array unchecked value
        var products = [];
        products = getFilteredProducts(this.props.products, brands, categories, colors )
        this.setState({
            brands: getBrands(products),
        })
        this.props.filterCategory(categories);
    }

    colorHandle(event, color, brands, categories){
        var index = color.indexOf(event.target.id);
        if(event.target.classList.contains('active')){
            event.target.classList.remove('active')
        }else {
            event.target.classList.add('active');
        }
        if (!(event.target.id in color) && event.target.classList.contains('active'))
            color.push(event.target.id); // push in array checked value
        else
            color.splice(index, 1); // removed in array unchecked value
        var products = [];
        products = getFilteredProducts(this.props.products, brands, categories, color)
        this.props.filterColor(color)
    }

    render (){
        const {translate} = this.props;
        return (
                <div className="collection-filter-block">
                    {/*brand filter start*/}
                    <div className="collection-mobile-back">
                        <span className="filter-back" onClick={(e) => this.closeFilter(e)} >
                            <i className="fa fa-angle-left" aria-hidden="true"></i> back
                        </span>
                    </div>
                    <SlideToggle>
                        {({onToggle, setCollapsibleElement}) => (
                            <div className="collection-collapse-block">
                                <h3 className="collapse-block-title" onClick={onToggle}>{translate('category')}</h3>
                                <div className="collection-collapse-block-content"  ref={setCollapsibleElement}>
                                    <div className="collection-brand-filter">
                                        {this.state.categories.map((category, index) => {
                                            return (
                                                <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                                                    <input type="checkbox" onClick={(e) =>
                                                        this.clickCategoryHandle(e,this.props.filters.category, this.props.filters.brand,
                                                            this.props.filters.color)} value={category} defaultChecked=
                                                        {this.props.filters.category.includes(category)? true : false}
                                                           className="custom-control-input category" id={category} />
                                                    <label className="custom-control-label"
                                                           htmlFor={category}>{category.replace('-', ' ').toUpperCase()}</label>
                                                </div> )
                                        })}
                                    </div>
                                </div>
                            </div>
                        )}
                    </SlideToggle>
                    <SlideToggle>
                        {({onToggle, setCollapsibleElement}) => (
                            <div className="collection-collapse-block">
                                <h3 className="collapse-block-title" onClick={onToggle}>{translate('brand')}</h3>
                                <div className="collection-collapse-block-content"  ref={setCollapsibleElement}>
                                    <div className="collection-brand-filter">
                                        {this.state.brands.map((brand, index) => {
                                            return (
                                                <div className="custom-control custom-checkbox collection-filter-checkbox" key={index}>
                                                    <input type="checkbox" onClick={(e) =>
                                                        this.clickBrandHandle(e,this.props.filters.brand, this.props.filters.category,
                                                            this.props.filters.color)} value={brand} defaultChecked=
                                                        {this.props.filters.brand.includes(brand)? true : false}
                                                           className="custom-control-input brand" id={brand} />
                                                    <label className="custom-control-label"
                                                           htmlFor={brand}>{brand}</label>
                                                </div> )
                                        })}
                                    </div>
                                </div>
                            </div>
                        )}
                    </SlideToggle>

                    {/*color filter start here*/}
                    <SlideToggle>
                        {({onToggle, setCollapsibleElement}) => (
                            <div className="collection-collapse-block">
                                <h3 className="collapse-block-title" onClick={onToggle}>{translate('colors')}</h3>
                                <div className="collection-collapse-block-content" ref={setCollapsibleElement}>
                                    <div className="color-selector">
                                        <ul>
                                            {this.state.colors.map((color, index) => {

                                                return (
                                                    <li className={`${color} ${this.props.filters.color.includes(color)?'active':''}`}
                                                        id={color} title={color} value={color} onClick={(e) =>
                                                        this.colorHandle(e, this.props.filters.color, this.props.filters.brand,
                                                            this.props.filters.category)} key={index}></li> )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        )}
                    </SlideToggle>
                    {/*price filter start here */}
                    <SlideToggle>
                        {({onToggle, setCollapsibleElement}) => (
                            <div className="collection-collapse-block open">
                                <h3 className="collapse-block-title" onClick={onToggle}>{translate('price')}</h3>
                                <div className="collection-collapse-block-content block-price-content" ref={setCollapsibleElement}>
                                    <div className="collection-brand-filter">
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <InputRange
                                                maxValue={this.props.prices.max}
                                                minValue={this.props.prices.min}
                                                value={this.props.filters.value}
                                                onChange={value => this.props.filterPrice({ value })} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </SlideToggle>
                </div>
        )
    }
}


const mapStateToProps = state => ({
    products: state.data.products,
    brands: getBrands(state.data.products),
    colors: getColors(state.data.products),
    prices: getMinMaxPrice(state.data.products),
    categories: getCategories(state.data.products),
    filters: state.filters
})

export default connect(
    mapStateToProps,
    { filterBrand, filterColor, filterPrice, filterCategory }
)(withTranslate(Filter));