import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import withTranslate from "react-redux-multilingual/lib/withTranslate";
import {Helmet} from "react-helmet";
import axios from 'axios';

class Contact extends Component {

    constructor (props) {
       super (props)

        this.state = {
            first_name: '',
            last_name: '',
            phone: '',
            email: '',
            message: ''
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        axios({
            method: "POST",
            url:"http://localhost:3002/send",
            data:  this.state
        }).then((response)=>{
            if (response.data.status === 'success'){
                alert("Message Sent.");
                this.resetForm()
            }else if(response.data.status === 'fail'){
                alert("Message failed to send.")
            }
        })
    }

    resetForm(){
        this.setState({first_name: '', last_name: '', phone: '', email: '', message: ''})
    }

    onFirstNameChange(event){
        this.setState({first_name: event.target.value})
    }

    onLastNameChange(event){
        this.setState({last_name: event.target.value})
    }

    onPhoneChange(event){
        this.setState({phone: event.target.value})
    }

    onEmailChange(event){
        this.setState({email: event.target.value})
    }

    onMessageChange(event){
        this.setState({message: event.target.value})
    }

    render (){

        const {translate} = this.props
        return (
            <div>
                {/*SEO Support*/}
                <Helmet>
                    <title>Eshop | Contact Us</title>
                    <meta name="description" content="Eshop" />
                </Helmet>
                {/*SEO Support End */}
                <Breadcrumb title={translate('contact_us')}/>
                
                
                {/*Forget Password section*/}
                <section className=" contact-page section-b-space">
                    <div className="container">
                        <div className="row section-b-space">
                            <div className="col-lg-7 map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2934.482975560137!2d18.089455816251714!3d42.65111957916844!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x134c752964c045e7%3A0x6ee4438aa510e956!2sVukovarska%20ul.%2013%2C%2020000%2C%20Dubrovnik!5e0!3m2!1sen!2shr!4v1598815579785!5m2!1sen!2shr"
                                    allowFullScreen></iframe>
                            </div>
                            <div className="col-lg-5">
                                <div className="contact-right">
                                    <ul>
                                        <li>
                                            <div className="contact-icon">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/icon/phone.png`} alt="Generic placeholder image" />
                                                    <h6>{translate('contact_us')}</h6>
                                            </div>
                                            <div className="media-body">
                                                <p>+385 986 - 754 - 3210</p>
                                                <p>+385 986 - 754 - 3211</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="contact-icon">
                                                <i className="fa fa-map-marker" aria-hidden="true"></i>
                                                <h6>{translate('address')}</h6>
                                            </div>
                                            <div className="media-body">
                                                <p>Vukovarska 13</p>
                                                <p>Dubrovnik 20000</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="contact-icon">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/icon/email.png`} alt="Generic placeholder image" />
                                                    <h6>{translate('email')}</h6>
                                            </div>
                                            <div className="media-body">
                                                <p>suport@eshop.com</p>
                                                <p>info@eshop.com</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="contact-icon">
                                                <i className="fa fa-fax" aria-hidden="true"></i>
                                                <h6>{translate('fax')}</h6>
                                            </div>
                                            <div className="media-body">
                                                <p>suport@eshop.com</p>
                                                <p>info@eshop.com</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <form className="theme-form" onSubmit={this.handleSubmit.bind(this)}>
                                    <div className="form-row">
                                        <div className="col-md-6">
                                            <label htmlFor="name">{translate('first_name')}</label>
                                            <input type="text" className="form-control" id="first_name"
                                                   placeholder={translate('first_name')} required="" value={this.state.first_name} onChange={this.onFirstNameChange.bind(this)}/>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="email">{translate('last_name')}</label>
                                            <input type="text" className="form-control" id="last_name"
                                                   placeholder={translate('last_name')} required="" value={this.state.last_name} onChange={this.onLastNameChange.bind(this)}/>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="review">{translate('phone_number')}</label>
                                            <input type="text" className="form-control" id="phone"
                                                   placeholder={translate('phone_number')} required="" value={this.state.phone} onChange={this.onPhoneChange.bind(this)}/>
                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor="email">{translate('email')}</label>
                                            <input type="text" className="form-control" id="email" placeholder={translate('email')}
                                                   required="" value={this.state.email} onChange={this.onEmailChange.bind(this)}/>
                                        </div>
                                        <div className="col-md-12">
                                            <label htmlFor="review">{translate('write_your_message')}</label>
                                            <textarea className="form-control" placeholder={translate('write_your_message')}
                                                      id="message" rows="6" value={this.state.message} onChange={this.onMessageChange.bind(this)}></textarea>
                                        </div>
                                        <div className="col-md-12">
                                            <button className="btn btn-solid" type="submit">{translate('send_your_message')}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default withTranslate(Contact);