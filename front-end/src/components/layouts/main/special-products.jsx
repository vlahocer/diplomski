import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {connect} from 'react-redux'

import {getBestSellerProducts, getNewProducts, getFeaturedProducts} from '../../../services/index'
import {addToCart, addToWishlist, addToCompare} from "../../../actions/index";
import ProductItem from './product-item';
import {withTranslate} from "react-redux-multilingual";

class SpecialProducts extends Component {
    render (){

        const {bestSeller,newProducts, featuredProducts, symbol, addToCart, addToWishlist, addToCompare, translate} = this.props

        return (
                <section className="section-b-space ratio_square">
                    <div className="container-fluid">
                        <div className="title2">
                            <h4>{translate('new_collection')}</h4>
                            <h2 className="title-inner2">{translate('trending_products')}</h2>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="theme-tab layout7-product">
                                    <Tabs className="theme-tab">
                                        <TabList  className="tabs tab-title">
                                            <Tab>{translate('new_arrival')}</Tab>
                                            <Tab>{translate('featured')}</Tab>
                                            <Tab>{translate('special')}</Tab>
                                        </TabList>

                                        <TabPanel>
                                            <div className="no-slider row">
                                                { newProducts.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                        <TabPanel>
                                            <div className="no-slider row">
                                                { featuredProducts.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                        <TabPanel>
                                            <div className=" no-slider row">
                                                { bestSeller.map((product, index ) =>
                                                    <ProductItem product={product} symbol={symbol}
                                                                 onAddToCompareClicked={() => addToCompare(product)}
                                                                 onAddToWishlistClicked={() => addToWishlist(product)}
                                                                 onAddToCartClicked={() => addToCart(product, 1)} key={index} /> )
                                                }
                                            </div>
                                        </TabPanel>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    bestSeller: getBestSellerProducts(state.data.products, ownProps.type),
    newProducts: getNewProducts(state.data.products, ownProps.type),
    featuredProducts: getFeaturedProducts(state.data.products, ownProps.type),
    symbol: state.data.symbol
})

export default connect(mapStateToProps, {addToCart, addToWishlist, addToCompare}) (withTranslate(SpecialProducts));