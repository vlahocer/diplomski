import React, { Component } from 'react';
import {Helmet} from 'react-helmet'
import '../../common/index.scss';
import Slider from 'react-slick';
// Import custom components
import SpecialProducts from "./special-products"
import {getHighestDiscount} from '../../../services'
import {connect} from "react-redux";
import withTranslate from "react-redux-multilingual/lib/withTranslate";
class Main extends Component {

    render(){
        const {discount,laptopDiscount,pcDiscount, translate} = this.props
        return (
            <div className="container-fluid">
                <Helmet>
                    <title>Eshop | Electronic Store</title>
                </Helmet>
                <section className="p-0 padding-bottom-cls">
                    <Slider className="slide-1 home-slider">
                        <div>
                            <div className="home home-1">
                                <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <div className="slider-contain">
                                                <div>
                                                    <h4>{translate('save')} {discount}% {translate('on')}</h4>
                                                    <h1 className="text-white-50">{translate('graphic_cards')}</h1>
                                                    <a href={`${process.env.PUBLIC_URL}/shop`} className="btn btn-outline
                                                    btn-classic text-white">{translate('shop_now')}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="home home-2">
                                <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <div className="slider-contain">
                                                <div>
                                                    <h4 className="text-white">{translate('save_up_to')}
                                                    {laptopDiscount}% {translate('on')}</h4>
                                                    <h1>{translate('laptops')}</h1>
                                                    <a href={`${process.env.PUBLIC_URL}/shop`} className="btn btn-outline
                                                    btn-classic text-white">{translate('shop_now')}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Slider>
                </section>

                <div className="layout-bg">
                    {/*About Section*/}
                    <section className="banner-goggles ratio2_3">
                        <div className="container-fluid">
                            <div className="row partition3">
                                <div className="col-md-4">
                                    <a href={`${process.env.PUBLIC_URL}/shop`}>
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/gpu.jpg`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 className="text-white">{discount}% {translate('off')}</h4>
                                                    <h2 className="text-white">{translate('graphic_cards')}</h2>
                                                </div></div></div></a>
                                </div>
                                <div className="col-md-4">
                                    <a href={`${process.env.PUBLIC_URL}/shop`}>
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/asus.jpg`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 className="text-white">{laptopDiscount}% {translate('off')}</h4>
                                                    <h2>{translate('laptops')}</h2>
                                                </div></div></div></a>
                                </div>
                                <div className="col-md-4">
                                    <a href={`${process.env.PUBLIC_URL}/shop`}>
                                        <div className="collection-banner">
                                            <div className="img-part">
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/electronics/pc.jpg`}
                                                     className="img-fluid blur-up lazyload bg-img" alt="" />
                                            </div>
                                            <div className="contain-banner banner-3">
                                                <div>
                                                    <h4 className="text-white">{pcDiscount}% {translate('off')}</h4>
                                                    <h2 className="text-white">{translate('gaming_pcs')}</h2>
                                                </div></div></div></a>
                                </div>
                            </div>
                        </div>
                    </section>
                    {/*About Section End*/}

                    {/*Product slider*/}
                    <SpecialProducts/>
                    {/*Product slider End*/}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    discount: getHighestDiscount(state.data.products, 'graphic-card'),
    laptopDiscount: getHighestDiscount(state.data.products, 'laptop'),
    pcDiscount: getHighestDiscount(state.data.products, 'computer')
})

export default connect(mapStateToProps, null) (withTranslate(Main));