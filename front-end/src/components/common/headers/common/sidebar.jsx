import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class SideBar extends Component {


    closeNav() {
        var closemyslide = document.getElementById("mySidenav");
        if (closemyslide)
            closemyslide.classList.remove('open-side');
    }

    handleSubmenu = (event) => {
        if (event.target.classList.contains('sub-arrow'))
            return;

        if(event.target.nextElementSibling.classList.contains('opensub1'))
            event.target.nextElementSibling.classList.remove('opensub1')
        else{
            document.querySelectorAll('.opensub1').forEach(function (value) {
                value.classList.remove('opensub1');
            });
            event.target.nextElementSibling.classList.add('opensub1')
        }
    }
    handleSubTwoMenu = (event) => {
        if (event.target.classList.contains('sub-arrow'))
            return;

        if(event.target.nextElementSibling.classList.contains('opensub2'))
            event.target.nextElementSibling.classList.remove('opensub2')
        else{
            document.querySelectorAll('.opensub2').forEach(function (value) {
                value.classList.remove('opensub2');
            });
            event.target.nextElementSibling.classList.add('opensub2')
        }
    }
    handleSubThreeMenu = (event) => {
        if (event.target.classList.contains('sub-arrow'))
            return;

        if(event.target.nextElementSibling.classList.contains('opensub3'))
            event.target.nextElementSibling.classList.remove('opensub3')
        else{
            document.querySelectorAll('.opensub3').forEach(function (value) {
                value.classList.remove('opensub3');
            });
            event.target.nextElementSibling.classList.add('opensub3')
        }
    }
    handleSubFourMenu = (event) => {
        if (event.target.classList.contains('sub-arrow'))
            return;

        if(event.target.nextElementSibling.classList.contains('opensub4'))
            event.target.nextElementSibling.classList.remove('opensub4')
        else{
            document.querySelectorAll('.opensub4').forEach(function (value) {
                value.classList.remove('opensub4');
            });
            event.target.nextElementSibling.classList.add('opensub4')
        }
    }

    handleMegaSubmenu = (event) => {
        if (event.target.classList.contains('sub-arrow'))
            return;

        if(event.target.nextElementSibling.classList.contains('opensidesubmenu'))
            event.target.nextElementSibling.classList.remove('opensidesubmenu')
        else{
            event.target.nextElementSibling.classList.add('opensidesubmenu')
        }
    }

    render() {
        return (
            <div id="mySidenav" className="sidenav">
                <a href="javascript:void(0)" className="sidebar-overlay" onClick={this.closeNav}></a>
                <nav>
                    <a onClick={this.closeNav}>
                        <div className="sidebar-back text-left">
                            <i className="fa fa-angle-left pr-2" aria-hidden="true"></i> Back
                        </div>
                    </a>
                    <ul id="sub-menu" className="sidebar-menu">
                        <li>
                            <Link to="#" onClick={(e) => this.handleMegaSubmenu(e)}>
                                računala
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>gaming računala</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">ADM gaming računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">NVIDIA gaming računala</Link>
                                                    </li>
                                                </ul>
                                                <h5>business računala</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">ADM business računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Brand name business računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">All in One business računala</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>uredska računala</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">ADM uredska računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Brand name uredska računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">All in One uredska računala</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <a href="#" className="mega-menu-banner">
                                            <img src={`${process.env.PUBLIC_URL}/assets/images/pc/pc-1.jpg`} alt="" className="img-fluid"/>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="#" onClick={(e) => this.handleSubmenu(e)}>
                                laptopi
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>gaming laptopi</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer gaming laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus gaming laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell gaming laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Lenovo gaming laptopi</Link>
                                                    </li>
                                                </ul>
                                                <h5>business laptopi</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer business laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Apple business laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus business laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell business laptopi</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>uredski laptopi</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer uredski laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus uredski laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell uredski laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HP uredski laptopi</Link>
                                                    </li>
                                                </ul>
                                                <h5>torbe i ruksaci za laptope</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Dell torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HP torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Lenovo torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Port torbe i ruskaci</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>dodatci i oprema za laptope</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Baterije za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dockovi i port replikatori za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Naponski adapteri (punjači) za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Hladnjaci za laptope</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="#" onClick={(e) => this.handleSubmenu(e)}>
                                monitori i projektori
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>gaming monitori</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer gaming monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus gaming monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell gaming monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Lenovo gaming monitori</Link>
                                                    </li>
                                                </ul>
                                                <h5>business monitori</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer business monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Apple business monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus business monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell business monitori</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>uredski monitori</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Acer uredski monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Asus uredski monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dell uredski monitori</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HP uredski monitori</Link>
                                                    </li>
                                                </ul>
                                                <h5>dodatci i oprema za laptope</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Baterije za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dockovi i port replikatori za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Naponski adapteri (punjači) za laptope</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Hladnjaci za laptope</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="#" onClick={(e) => this.handleSubmenu(e)}>
                                komponente
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>procesori - cpu</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">CPU Intel s.2011</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU Intel s.2066</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU Intel ostali</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU AMD s.AM4</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU AMD s.TR4</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU Intel s.1200</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">CPU AMD s.SP3</Link>
                                                    </li>
                                                </ul>
                                                <h5>hladnjaci cpu i vga</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Hladnjaci CPU</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Hladnjaci VGA</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Termalne paste i padovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Razni ventilatori i hladila</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Vodena hlađenja za CPU</Link>
                                                    </li>
                                                </ul>
                                                <h5>memorija</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Memorije računala</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Memorija laptopi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Memorija serveri</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>matične ploče - mbo</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">MBO Intel s.1151</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO Intel s.2011</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO Intel s.2066</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO Intel ostali</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO AMD s.AM4</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO AMD s.TR4 i s.TRX4</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">MBO Intel s.1200</Link>
                                                    </li>
                                                </ul>
                                                <h5>torbe i ruksaci za laptope</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Dell torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HP torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Lenovo torbe i ruskaci</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Port torbe i ruskaci</Link>
                                                    </li>
                                                </ul>
                                                <h5>grafičke kartice - gfx</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">GFX nVidia</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">GFX AMD</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">GFX Quadro</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>pohrana podataka</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Pohrana DVDRW</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana Blu Ray</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana HDD 3.5"</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana HDD 2.5"</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana SSD SATA</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana SSD M.2</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana dodatna oprema</Link>
                                                    </li>
                                                </ul>
                                                <h5>pohrana podataka</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Pohrana DVDRW</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana Blu Ray</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana HDD 3.5"</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana HDD 2.5"</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana SSD SATA</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana SSD M.2</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Pohrana dodatna oprema</Link>
                                                    </li>
                                                </ul>
                                                <h5>kućišta</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">ITX kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Mini tower kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Midi tower kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Full tower kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Ventilatori za kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Dodaci za kućišta</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Rack kućišta</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="#" onClick={(e) => this.handleSubmenu(e)}>
                                vanjska pohrana podataka
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>VANJSKI DISKOVI - POHRANA</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">SSD- vanjska pohrana</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HDD 2.5"- vanjska pohrana</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HDD 3.5"- vanjska pohrana</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>Kućišta za vanjsku pohranu podataka</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">DVD/Blueray vanjska pohrana</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">USB STICKOVI</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">USB 2.0 - stickovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">USB 3.0, 3.1 - stickovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">USB Mini, Micro - stickovi</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>MEMORIJSKE KARTICE</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Secure digital kartice</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Compact flash kartice</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Čitači kartica</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="#" onClick={(e) => this.handleSubmenu(e)}>
                                kablovi
                                <span className="sub-arrow"></span>
                            </Link>
                            <ul className="mega-menu">
                                <li>
                                    <div className="row m-0">
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>KABLOVI ZA MONITORE</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">VGA kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">DVI kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">HDMI kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Display port kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Combo DVI/VGA kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Combo HDMI/DVI kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Combo Display port/HDMI kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Ostali kablovi</Link>
                                                    </li>
                                                </ul>
                                                <h5>mrežni kablovi</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Mrežni kablovi (UTP, FTP, STP)</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Crossover kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Optički kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Null modem kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Oprema za mrežne kablove</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>kablovi za pisače</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">USB kablovi za pisače</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">USB adapteri za pisače</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Paralelni kablovi za pisače</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">SATA HDD/SSD DATA KABLOVI</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">SATA HDD/SSD data kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">eSATA HDD/SSD data kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">SAS HDD/SSD data kablovi</Link>
                                                    </li>
                                                </ul>
                                                <h5>audio kablovi</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Optički audio kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Ostali audio kablovi</Link>
                                                    </li>
                                                </ul>
                                                <h5>usb kablovi</h5>Canyon USB kablovi
                                                <ul>
                                                    <li>
                                                        <Link to="#">Rollline USB kablovi</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Ostali USB kablovi</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="link-section">
                                                <h5>kablovi za napajanja</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Kablovi za napajanja šuko</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Kablovi za napajanja SATA</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Kablovi za napajanja ostali</Link>
                                                    </li>
                                                </ul>
                                                <h5>adapteri za monitore</h5>
                                                <ul>
                                                    <li>
                                                        <Link to="#">Adapter VGA</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Adapter DVI</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Adapter HDMI</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Adapter Dislay Port</Link>
                                                    </li>
                                                    <li>
                                                        <Link to="#">Ostali adapteri</Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

        )
    }
}


export default SideBar;