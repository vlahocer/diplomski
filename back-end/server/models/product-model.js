
        const mongoose = require('mongoose')
        require('mongoose-double')(mongoose);
        const Schema = mongoose.Schema
        var SchemaTypes = mongoose.Schema.Types;

        const Products = new Schema(
            {
                name: { type: String, required: true },
                price: { type: SchemaTypes.Double, required: true },
                salePrice: { type: SchemaTypes.Double, required: false },
                discount: { type: Number, required: false },
                pictures: { type: [String], required: false },
                shortDetails: { type: String, required: false },
                description: { type: String, required: false },
                stock: { type: Number, required: false },
                new: { type: Boolean, required: true, default: true },
                sale: { type: Boolean, required: true, default: false },
                category: { type: [String], required: true },
                colors: { type: [String], required: true },
                tags: { type: [String], required: true },
                variants: {
                    color: String,
                    images: String
                },
            },
        )

        module.exports = mongoose.model('product', Products, 'product')