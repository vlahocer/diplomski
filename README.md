Upute za pokretanje na serveru

Odpakirati build.zip i sve fileove i foldere iz build foldera staviti u public html servera,

Instalirati mongodb na vaš server --- tutorial za instalaciju mongodb ekstenzije u cpanel --
http://www.mochasupport.com/kayako/index.php?_m=knowledgebase&_a=viewarticle&kbarticleid=814&ratingconfirm=1

Kreirati bazu electronic_store i importati podatke iz product.json,
za kreiranje baze pratite sljedeći link -- https://www.mongodb.com/basics/create-database,
za import podataka pratiti sljedeći link -- https://docs.mongodb.com/guides/server/import/.

Za pokretanje mail servera pratite sljedeći link -- https://www.smarthost.eu/blog/how-to-run-node-js-in-cpanel,
mail file je u src folderu pod nazivom sendMail.js.

Upute za pokretanje lokalno

Klonirajte diplomski rad sa linka -- git clone https://vlahocer@bitbucket.org/vlahocer/diplomski.git,
instalirajte package pomoću node-a ili yarn-a. (npm install, yarn install)
 
Instalirajte mongodb compass comunity https://www.mongodb.com/try/download/compass, 
te stvorite bazu electronic_store i importajte product.json preko compass programa,

Otvorite novi terminal.
Za pokretanje backend servera navigirajte u back-end -> server te u terminalu upišite node index.js, 
Trebali bi vidjeti poruku "Server running on port 3000" ako je sve u redu.

Otvorite novi terminal.
Za pokretanje mail servera, navigirajte u front-end -> src te u terminalu upišite node sendMail.js.
Trebali bi vidjeti poruku "Server is ready to take messages" ako je sve u redu.

Otvorite novi terminal.
Za pokretanje aplikacije navigirajte u front-end folder te u terminalu upišite npm start.
Server bi se trebao pokrenuti na localhost:8000 adresi.